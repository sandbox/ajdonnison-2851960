# SlideShare plugin for Video Embed Field

This provides a SlideShare plugin that works with the [Video Embed Field](https://drupal.org/project/video_embed_field) module.

## Installation

Place in your preferred modules directory for Drupal 8.x (typically drupalroot/modules/custom/) and enable it as usual.  Once done you can then enable Slideshare as an option in the Video Embed Field configuration.
