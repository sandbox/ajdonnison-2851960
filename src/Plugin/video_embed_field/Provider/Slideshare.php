<?php

namespace Drupal\video_embed_slideshare\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use Drupal\Core\Url;
use Symfony\Component\DomCrawler\Crawler;

/**
 * A Slideshare provider plugin.
 *
 * @VideoEmbedProvider(
 *    id = "slideshare",
 *    title = @Translation("Slideshare")
 * )
 */
class Slideshare extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $embed_data  = $this->oEmbdedData($width, $height);
    return [
      '#type' => 'video_embed_iframe',
      '#provider' => $embed_data->provider,
      '#url' => $this->getUrlFromDom($embed_data->html),
      '#attributes' => [
        'width' => $width,
	'height' => $height,
	'frameborder' => '0',
	'allowfullscreen' => 'allowfullscreen',
      ],
    ];
  }

  /**
   * Find the src url from the markup provided.
   */
  protected function getUrlFromDom($html) {
    $crawler = new Crawler($html);
    return $crawler->filter('iframe')->attr('src');
  }

  /**
   * Grab (and store) oEmbed data allowing us to answer questions about the slideshow
   */
  protected function oEmbdedData($width=NULL, $height=NULL) {
    $query = [ 
      'format' => 'json',
       'url' => 'http://www.slideshare.net/' . $this->getVideoId(),
    ];
    if ($width) {
      $query['maxwidth'] = $width;
    }
    if ($height) {
      $query['maxheight'] = $height;
    }
    $uri = Url::fromUri('http://www.slideshare.net/api/oembed/2', [ 'query' => $query ])->toString();
    return json_decode(file_get_contents($uri));
  }
  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return $this->oEmbedData()->thumbnail;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    // Slideshare uses the URL as the Id
    preg_match('_^https?://(www\.)?slideshare.net/(?P<provider>[^/]+)/(?P<video>[^/]+)$_', $input, $matched);
    return (isset($matched['provider']) && isset($matched['video'])) ? ($matched['provider'] . '/'. $matched['video']) : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->oEmbedData()->title;
  }
}
